const mongoose = require("mongoose");
const autoIncrement = require("mongoose-auto-increment");

const _ = require("lodash");
const OrderConstant = require("../../order/constants/orderConstant");

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
	{
		id: {
			type: Number,
			required: true,
			unique: true,
		},
		accountId: {
			type: Number,
			require: true,
		},
		service: {
			type: { type: String },
			id: { type: Number },
			state: { type: String, enum: _.values(OrderConstant.STATE) },
			name: { type: String },
			data: {
				balanceBefore: { type: Number },
				balanceAfter: { type: Number },
				fromWallet: { type: Number },
				napasUrl: { type: String },
				transactionId: { type: String },
				partnerTransactionId: { type: String },
			},
		},
		amount: {
			type: Number,
			required: true,
			default: null,
		},
		fee: {
			type: Number,
			required: false,
			default: null,
		},
		total: {
			type: Number,
			required: true,
			default: null,
		},
		description: {
			type: String,
			default: "",
		},
	},
	{
		collection: "History",
		versionKey: false,
		timestamps: true,
	}
);

Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: "id",
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
