const { MoleculerError } = require("moleculer").Errors;
const OrderConstant = require("../../order/constants/orderConstant");
const Numeral = require("numeral");
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { accountId, orderData, type } = ctx.params;

		const historyObj = {
			accountId,
			service: {
				type,
				id: orderData.order.id,
				state: orderData.order.state,
				name: "Thanh toán order",
				data:
					type === "WALLET"
						? {
								balanceBefore: orderData.balanceBefore,
								balanceAfter: orderData.balanceAfter,
								fromWallet: orderData.fromWallet,
								transactionId: orderData.transactionId,
						  }
						: {
								napasUrl: orderData.link,
								transactionId: orderData.transactionId,
								partnerTransactionId: orderData.partnerTransactionId
						  },
			},
			amount: orderData.order.amount,
			fee: orderData.order.fee,
			total: orderData.order.total,
			state: "PENDING",
			description: `Thanh toán ${Numeral(orderData.order.amount).format(
				"0,0"
			)} VND`,
		};

		const history = await this.broker.call("v1.HistoryModel.create", [
			historyObj,
		]);

		if (_.get(history, "id", null) === null) {
			return {
				code: 1000,
				message: "Fail to create history",
			};
		}

		return history;
	} catch (err) {
		console.log(err);
		throw new MoleculerError(`[History] Create history: ${err.message}`);
	}
};
