const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});
const path = require('path')
const moleculerI18n = require("moleculer-i18n-js");

module.exports = {
	name: "Customer",
	mixins: [AuthMixin, QueueMixin, moleculerI18n],

	version: 1,
	i18n: {
		directory: path.join(__dirname, "locales"),
		locales: ["vi", "en"],
		defaultLocale: 'vi'
	},

	settings: {},

	hooks: {
		before: {
			// "get-*": ["checkIsAuthenticated"],
		},
	},

	actions: {
		get: {
			rest: {
				method: "GET",
				fullPath: "/v1/user-info",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {},

			handler: require("./actions/getUserInfomation.action"),
		},
		editUserInfo: {
			rest: {
				method: "PUT",
				fullPath: "/v1/user-info/:accountId",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					email: "string",
					firstName: "string",
					lastName: "string",
					gender: "string",
					avatar: "string",
				},
			},
			handler: require("./actions/editUserInfomation.action"),
		},
		getWalletInfo: {
			rest: {
				method: "GET",
				fullPath: "/v1/balance",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {},
			handler: require("./actions/getUserBalance.action"),
		},
		editWalletBalance: {
			rest: {
				method: "PUT",
				fullPath: "/v1/External/MiniProgram/:accountId/Balance",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			params: {
				body: {
					$$type: "object",
					amount: "number",
					action: "string",
				},
			},
			handler: require("./actions/editUserBalance.action"),
		},
		sendOtp: {
			rest: {
				method: "POST",
				fullPath: "/v1/sendOtp/",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					orderId: "number",
				},
			},
			handler: require("./actions/sendOtp.action"),
		},
		payOrder: {
			rest: {
				method: "POST",
				fullPath: "/v1/pay/",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					otp: "string",
				},
			},
			handler: require("./actions/pay.action"),
		},
		getOrder: {
			rest: {
				method: "GET",
				fullPath: "/v1/get-order",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					orderId: "number"
				}	
			},
			handler: require('../order/actions/getOrder.action')
		},
	},

	created() {},

	async started() {},

	async stopped() {},
};
