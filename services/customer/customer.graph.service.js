const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});
const path = require("path");
const moleculerI18n = require("moleculer-i18n-js");

module.exports = {
	name: "Customer.graph",
	mixins: [AuthMixin, QueueMixin, moleculerI18n],

	version: 1,
	i18n: {
		directory: path.join(__dirname, "locales"),
		locales: ["vi", "en"],
		defaultLocale: "vi",
	},

	settings: {
		graphql: {
			type: require("../graph/type"),
			input: require("../graph/input"),
			resolvers: {
				CustomerMutation: {
					SendOtp: {
						action: "v1.Customer.graph.sendOtp",
					},
                    PayOrder: {
                        action: "v1.Customer.graph.payOrder"
                    },
					CreateOrder: { 
						action: "v1.Order.graph.create"
					},
				},
				CustomerQuery: {
					GetOrder: { 
						action: "v1.Order.graph.get"
					},
					GetUserInfo: {
						action: "v1.Customer.graph.get"
					}
				},
			},
		},
	},

	actions: {
		get: {
			graphql: {
                query: "CustomerQuery: CustomerQuery",
                mutation: "CustomerMutation: CustomerMutation"
            },

			handler: require("./actions/getUserInfomation.action"),
		},
		editUserInfo: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					email: "string",
					firstName: "string",
					lastName: "string",
					gender: "string",
					avatar: "string",
				},
			},
			handler: require("./actions/editUserInfomation.action"),
		},
		getWalletInfo: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {},
			handler: require("./actions/getUserBalance.action"),
		},
		editWalletBalance: {
			params: {
				body: {
					$$type: "object",
					amount: "number",
					action: "string",
				},
			},
			handler: require("./actions/editUserBalance.action"),
		},
		sendOtp: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					orderId: "number",
				},
			},
			handler: require("./actions/sendOtp.action"),
		},
		payOrder: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					otp: "string",
				},
			},
			handler: require("./actions/pay.action"),
		},
	},

	created() {},

	async started() {},

	async stopped() {},
};
