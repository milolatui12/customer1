const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

const OrderConstant = require("../../order/constants/orderConstant");
const { customAlphabet } = require("nanoid");
const { alphanumeric } = require("nanoid-dictionary");
const nanoId = customAlphabet(alphanumeric, 15);

module.exports = async function (ctx) {
	const { otp } = ctx.params.body;

	const sessionId = ctx.meta.session;
	const { accountId } = await this.broker.call("v1.SessionModel.findOne", [
		{ id: sessionId },
	]);

	try {
		const otpObj = await this.broker.call("v1.Otp.verify", {
			otp,
			accountId,
		});

		if (_.get(otpObj, "code", null) !== null) {
			return {
				code: 1001,
				successed: false,
				message: otpObj.message
			};
		}

		let orderUnlock;
		let data = {};
		let transactionId;
		try {
			orderUnlock = await this.broker.cacher.lock(
				`order_${otpObj.data.orderId}`
			);

			const order = await this.broker.call("v1.OrderModel.findOne", [
				{
					id: otpObj.data.orderId,
					state: OrderConstant.STATE.PENDING,
				},
			]);

			if (_.get(order, "id", null) === null) {
				return {
					code: 1001,
					successed: false,
					message: "Order not exists!",
				};
			}

			transactionId = nanoId();
			///CREATE PAYMENT
			const paymentObj = {
				orderId: order.id,
				transactionId,
				method: order.payment.method,
				state: "PENDING",
			};

			const payment = await this.broker.call("v1.PaymentModel.create", [
				paymentObj,
			]);
			if (_.get(payment, "id", null) === null) {
				return {
					code: 1001,
					successed: false,
					message: "Fail to pay the order!",
				};
			}
			///CREATE PAYMENT
			switch (order.payment?.method) {
				case OrderConstant.PAYMENT_METHOD.WALLET:
					data = await this.broker.call("v1.Payment.payWithWallet", {
						accountId,
						order,
						transactionId,
					});
					break;
				case OrderConstant.PAYMENT_METHOD.ATM_CARD:
					data = await this.broker.call("v1.Payment.payWithATM", {
						accountId,
						order,
						transactionId,
					});
					break;
				default:
					break;
			}
		} catch (error) {
			await this.broker.call("v1.PaymentModel.updateOne", [
				{
					transactionId,
					state: "PENDING",
				},
				{
					state: "FAILED",
				},
			]);
			throw new Error(error);
		} finally {
			if (_.isFunction(orderUnlock)) {
				await orderUnlock();
			}
		}

		return data;
	} catch (err) {
		throw new MoleculerError(`Pay order: ${err.message}`);
	} finally {
		await this.broker.call("v1.PayOrderOtpModel.findOneAndUpdate", [
			{
				OTP: otp,
				accountId,
				isValid: true,
			},
			{
				isValid: false,
			},
		]);
	}
};
