const { MoleculerError } = require("moleculer").Errors;
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const moment = require("moment");

module.exports = async function (ctx) {
	try {
		const { firstName, lastName, gender, avatar } = ctx.params.body;

		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		const accountIdParam = ctx.params.params.accountId;

		if (!(accountId == accountIdParam)) {
			return {
				code: 1001,
				message: "No permission",
			};
		}

		let accountInfo = await this.broker.call("v1.AccountModel.findOne", [
			{
				id: accountId,
			},
		]);

		if (_.get(accountInfo, "id", null) === null) {
			return {
				code: 1001,
				message: "Không tồn tại account này",
			};
		}

		accountInfo = await this.broker.call(
			"v1.AccountModel.findOneAndUpdate",
			[
				{ id: accountId },
				{
					firstName: firstName || accountInfo.firstName,
					lastName: lastName || accountInfo.lastName,
					gender: gender || accountInfo.gender,
					avatar: avatar || accountInfo.avatar,
				},
			]
		);

		if (_.get(accountInfo, "id", null) === null) {
			return {
				code: 1001,
				message: "Fail",
			};
		}

		return {
			code: 1000,
			message: "Successful",
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[Customer] Get User Infomation: ${err.message}`
		);
	}
};
