const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		const userInfo = {};
		let unlock;
		try {
			unlock = await this.broker.cacher.lock(`account_${accountId}`);
			const accountInfo = await this.broker.call(
				"v1.AccountModel.findOne",
				[{ id: accountId }]
			);

			if (_.get(accountInfo, "id", null) === null) {
				return {
					code: 1001,
					message: "Account does not existed",
				};
			}

			userInfo.fullName =
				accountInfo.lastName + " " + accountInfo.firstName;
			userInfo.email = accountInfo.email;
			userInfo.phone = accountInfo.phone;
			userInfo.gender = accountInfo.gender;
			userInfo.avatar = null;
		} catch (err) {
			throw new MoleculerError(err);
		} finally {
			if (_.isFunction(unlock)) {
				await unlock();
			}
		}

		return {
			code: 1000,
			message: "Lấy thông tin thành công",
			userInfo,
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[Customer] Get User Infomation: ${err.message}`
		);
	}
};
