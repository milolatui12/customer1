const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const moment = require("moment");

const otpGenerator = require("otp-generator");

const OrderConstant = require("../../order/constants/orderConstant");

module.exports = async function (ctx) {
	try {
		const { orderId } = ctx.params.body;
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		const { email } = await this.broker.call("v1.AccountModel.findOne", [
			{ id: accountId },
		]);

		const order = await this.broker.call("v1.OrderModel.findOne", [
			{
				id: orderId,
				state: OrderConstant.STATE.PENDING,
			},
		]);

		if (_.get(order, "id", null) === null) {
			return {
				code: 1001,
				successed: false,
				message: "Order not found",
			};
		}

		const data = {
			orderId,
		};

		const otp = await this.broker.call("v1.Otp.create", {
			accountId: accountId,
			data,
			type: "PAY_ORDER",
		});

		/* await this.broker.call("v1.Mail.send.async", {
			params: {
				subject: "Pay Order Otp",
				text: "Your OTP for pay order: " + otp,
				email: email,
			},
		}); */

		return {
			code: 1000,
			successed: true,
			message: `OTP was send "${otp}`,
		};
	} catch (err) {
		console.log(err);
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[Paymen] Pay order: ${err.message}`);
	}
};
