const { MoleculerError } = require("moleculer").Errors;
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const moment = require("moment");

module.exports = async function (ctx) {
	try {
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		const userInfo = {};
		const walletInfo = await this.broker.call(
			"v1.UserWalletModel.findOne",
			[{ accountId }]
		);

		if (_.get(walletInfo, "id", null) === null) {
			return {
				code: 1001,
				data: {
					message: "Không tồn tại người dùng này",
				},
			};
		}

		userInfo.balance = walletInfo.balance;

		return {
			code: 1000,
			data: {
				message: "Lấy thông tin thành công",
				userInfo,
			},
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[Customer] Get User Infomation: ${err.message}`
		);
	}
};
