const { MoleculerError } = require("moleculer").Errors;
const jwt = require("jsonwebtoken");
const _ = require("lodash");
const moment = require("moment");
const mongoose = require('mongoose')

const session = mongoose.startSession

module.exports = async function (ctx) {
	try {
		const { amount, action } = ctx.params.body;
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		let walletInfo = await this.broker.call(
			"v1.UserWalletModel.findOne",
			[{ accountId }]
		);

        if (_.get(walletInfo, "id", null) === null) {
			return {
				code: 1001,
				data: {
					message: "Không tồn tại người dùng này",
				},
			};
		}

		if(action === "sub" && walletInfo.balance < amount) {
            return {
				code: 1001,
				data: {
					message: "Số dư không đủ",
				},
			};
        }

		await this.broker.waitForServices()

        const newBalance = action === "add"? walletInfo.balance + amount: walletInfo.balance - amount

		setTimeout(async () => {walletInfo = await this.broker.call("v1.UserWalletModel.findOneAndUpdate", [{
            accountId
        }, {
            balance: newBalance
        }])}, 5000)

		return {
			code: 1000,
			message: "Thành công",
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(
			`[Customer] Get User Infomation: ${err.message}`
		);
	}
};
