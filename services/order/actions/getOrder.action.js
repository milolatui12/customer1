const { MoleculerError } = require("moleculer").Errors;
// const OrderConstant = require("../../order/constants/orderConstant");
const Numeral = require("numeral");
const _ = require("lodash");

module.exports = async function (ctx) {
	try {
		const { orderId } = ctx.params.body;
		const sessionId = ctx.meta.session;
		const { accountId } = await this.broker.call(
			"v1.SessionModel.findOne",
			[{ id: sessionId }]
		);

		let order;
		let unlock;
		try {
			unlock = await this.broker.cacher.lock(`order_${orderId}`);
			order = await this.broker.call("v1.OrderModel.findOne", [
				{ id: orderId, accountId },
			]);

			if (_.get(order, "id", null) === null) {
				return {
					code: 1001,
					message: "No permission or order not existed",
				};
			}
		} catch (error) {
			throw new Error("Can't get order with orderId");
		} finally {
			if (_.isFunction(unlock)) {
				unlock();
			}
		}

		return {
			code: 1000,
			message: "Get order successful",
			order,
		};
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[Order] Get Order: ${err.message}`);
	}
};
