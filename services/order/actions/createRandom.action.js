const { MoleculerError } = require("moleculer").Errors;
const Numeral = require("numeral");
const moment = require("moment");
const _ = require("lodash");

const { customAlphabet } = require("nanoid");
const { alphanumeric } = require("nanoid-dictionary");
const nanoId = customAlphabet(alphanumeric, 15);

const awaitAsyncForeach = require("await-async-foreach");

module.exports = async function (ctx) {
	try {
		// const a = await this.broker.call("v1.OrderModel.deleteMany", [])
		// console.log(a);
		// const b = await this.broker.call("v1.PaymentModel.deleteMany", [])
		// console.log(b);

		await awaitAsyncForeach(Array.from(Array(1000).keys()), async () => {
			const accountId = Math.floor(Math.random() * (1000 - 3)) + 1;

			const a = [10000, 500000, 20000, 10000000];
			const amount = a[Math.floor(Math.random() * a.length)];

			const paymentArr = ["WALLET", "ATM_CARD"];
			const paymentMethod =
				paymentArr[Math.floor(Math.random() * paymentArr.length)];

			const fee = Math.floor(Math.random() * 5) * 0.01;
			const description = `pay ${Numeral(amount).format(
				"0,0"
			)}. With ${fee} fee`;
			const notes = "";

			const s = ["PENDING", "SUCCEEDED", "FAILED"];
			const state = s[Math.floor(Math.random() * s.length)];
            const interval = Math.floor(Math.random() * 2000) + 1

			const createdAt = moment(new Date()).subtract(interval, "day");

			const orderObj = {
				accountId,
				amount,
				fee,
				total: amount + amount * fee,
				state,
				description,
				notes,
				payment: {
					method: paymentMethod,
					state: "PENDING",
				},
				expiredAt: moment(createdAt).add(2, "hour"),
				createdAt: createdAt,
				updatedAt: createdAt,
			};

			const order = await this.broker.call("v1.OrderModel.create", [
				orderObj,
			]);

			transactionId = nanoId();
			const paymentObj = {
				orderId: order.id,
				transactionId,
				method: paymentMethod,
				state: state,
				createdAt: createdAt,
				updatedAt: createdAt,
			};

			const payment = await this.broker.call("v1.PaymentModel.create", [
				paymentObj,
			]);
			if (_.get(payment, "id", null) === null) {
				return {
					code: 1001,
					successed: false,
					message: "Fail to pay the order!",
				};
			}

			if (_.get(order, "id", null) === null) {
				return {
					code: 1000,
					successed: false,
					message: "Fail to create order",
				};
			}
		});

		console.log("a");
		return "a";
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[Order] Create order: ${err.message}`);
	}
};
