const { MoleculerError } = require("moleculer").Errors;
const OrderConstant = require("../../order/constants/orderConstant");
const _ = require("lodash");
const moment = require("moment");
const awaitAsyncForeach = require("await-async-foreach");

const axios = require("axios").default;

module.exports = async function (ctx) {
	try {
		let orders = await this.broker.call("v1.OrderModel.findMany", [
			{ state: OrderConstant.STATE.PENDING },
			null,
			{ limit: 30 },
		]);

		const now = new Date();
		let unlock;

		orders = orders.filter(
			(order) => moment(now).isAfter(order.expiredAt)
		);

		await awaitAsyncForeach(orders, async (order) => {
			try {
				unlock = await this.broker.cacher.lock(`order_${order.id}`);
				let state = OrderConstant.STATE.EXPIRED;
				if (
					order.payment.method ===
					OrderConstant.PAYMENT_METHOD.ATM_CARD
				) {
					const res = await axios.get(
						"http://localhost:3002/get-payment-state",
						{
							transactionId: order.transaction,
						}
					);
					if (
						!!res.data.data &&
						res.data.state !== OrderConstant.STATE.PENDING
					) {
						state = res.data.state;					
					}
				}
				await this.broker.call("v1.OrderModel.updateOne", [
					{
						id: order.id,
					},
					{
						state: state,
					},
				]);
			} catch (error) {
				console.log(error.message, "1");
			} finally {
				if (_.isFunction(unlock)) {
					await unlock();
				}
			}
		});
	} catch (err) {
		if (err.name === "MoleculerError") throw err;
		throw new MoleculerError(`[Order] Expired order: ${err.message}`);
	}
};
