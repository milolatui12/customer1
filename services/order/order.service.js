const { MoleculerError } = require("moleculer").Errors;
const _ = require("lodash");
const AuthMixin = require("../../mixins/authorize.mixin");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Order",
	version: 1,

	mixins: [AuthMixin, QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		create: {
			rest: {
				method: "POST",
				fullPath: "/v1/create-order",
				auth: {
					strategies: ['default'],
					mode: "required", // 'required', 'optional', 'try'
				},
			},
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					amount: "number",
					description: "string",
					notes: "string",
					paymentMethod: "string",
				},
			},
			handler: require("./actions/createOrder.action"),
		},

		expiredOrder: {
			handler: require("./actions/expiredOrder.action"),
		},

		random: {
			handler: require("./actions/createRandom.action")
		}
	},
	methods: {},
};
