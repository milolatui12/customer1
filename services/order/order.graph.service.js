const AuthMixin = require("../../mixins/authorize.mixin");
const _ = require("lodash");

const QueueMixin = require("moleculer-rabbitmq")({
	connection: "amqp://localhost",
	asyncActions: true, // Enable auto generate .async version for actions
});

module.exports = {
	name: "Order.graph",
	version: 1,

	mixins: [AuthMixin, QueueMixin],

	settings: {
		amqp: {
			connection: "amqp://127.0.0.1", // You can also override setting from service setting
		},
	},
	actions: {
		create: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					amount: "number",
					description: "string",
					notes: "string",
					paymentMethod: "string",
				},
			},
			handler: require("./actions/createOrder.action"),
		},
		get: {
			hooks: {
				before: ["checkIsAuthenticated"],
			},
			params: {
				body: {
					$$type: "object",
					orderId: "number",
				},
			},
			handler: require("./actions/getOrder.action"),
		},
		expiredOrder: {
			handler: require("./actions/expiredOrder.action"),
		},
	},
	methods: {},
};
