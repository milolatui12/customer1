const mongoose = require("mongoose");
const autoIncrement = require('mongoose-auto-increment');

const _ = require("lodash");

const WalletConstant = require('../constants/wallet.constant')

autoIncrement.initialize(mongoose);

const Schema = mongoose.Schema(
    {
		id: {
			type: Number,
			required: true,
			unique: true,
		},
        accountId: {
            type: Number,
            require: true,
            unique: true
        },
        balance: {
            type: Number,
            require: true,
        },
		currency: {
			type: String,
			default: "VND"
		},
	},
	{
		collection: "UserWallet",
		versionKey: false,
		timestamps: true,
	}
);


Schema.plugin(autoIncrement.plugin, {
	model: `${Schema.options.collection}-id`,
	field: 'id',
	startAt: 1,
	incrementBy: 1,
});

module.exports = mongoose.model(Schema.options.collection, Schema);
