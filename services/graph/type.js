const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	type CustomerMutation {
		SendOtp(body: SendOtpInput!): SendOtpResponse
		PayOrder(body: PayOrderInput!): PayOrderResponse
		CreateOrder(body: CreateOrderInput!): CreateOrderResponse
	}
	type CustomerQuery {
		GetOrder(body: GetOrderInput!): GetOrderResponse
		GetUserInfo: GetUserInfoResponse
	}
	type SendOtpResponse {
		successed: Boolean
		message: String
	}
	type PayOrderResponse {
		successed: Boolean
		message: String
		link: String
	}

	type CreateOrderResponse {
		successed: Boolean
		message: String
		orderId: Int
	}

	type GetOrderResponse {
		order: Order
	}

	type Order {
		transaction: String
		fee: Float
		total: Float
		amount: Float
		description: String
		notes: String
		state: String
		payment: Payment
	}

	type Payment {
		method: String
		state: String
	}

	type GetUserInfoResponse {
		userInfo: User
	}

	type User {
		fullName: String
		email: String
		phone: String
		gender: String
		avatar: String
	}
`;
