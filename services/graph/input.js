const gql = require("moleculer-apollo-server").moleculerGql;

module.exports = gql`
	input SendOtpInput {
		orderId: Int!
	}
	input PayOrderInput {
		otp: String!
	}
	input CreateOrderInput {
		amount: Int!
		description: String!
		notes: String!
		paymentMethod: String!
	}

	input GetOrderInput {
		orderId: Int!
	}
`;
